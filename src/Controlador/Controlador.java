package Controlador;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import vista.*;
import Modelo.*;
import Vista.vistaIngreso;
import Vista.vistaLista;
import Vista.vistaRegistro;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.lang.Exception;

public class Controlador implements ActionListener {

    private vistaIngreso vistaIngre;
    private vistaLista vistaList;
    private vistaRegistro vistaRegis;
    private Usuarios ModelUs;
    private dbManejador ModeldbManejador;
    private dbUsuarios ModelbdUsuarios;
    private boolean isInsertar = true;

    public Controlador(vistaIngreso vistaIngre, vistaLista vistaList, vistaRegistro vistaRegis, Usuarios ModelUs, dbManejador ModeldbManejador, dbUsuarios ModelbdUsuarios) {
        this.vistaIngre = vistaIngre;
        this.vistaList = vistaList;
        this.vistaRegis = vistaRegis;
        this.ModelUs = ModelUs;
        this.ModeldbManejador = ModeldbManejador;
        this.ModelbdUsuarios = ModelbdUsuarios;

        vistaIngre.btnIngresar.addActionListener(this);
        vistaIngre.btnRegistrar.addActionListener(this);
        vistaIngre.passContraseña.addActionListener(this);
        vistaIngre.txtUsuario.addActionListener(this);
        vistaList.btnCerrar2.addActionListener(this);
        vistaRegis.TxtCorreoElectronico.addActionListener(this);
        vistaRegis.btnCerrar.addActionListener(this);
        vistaRegis.btnGuardar.addActionListener(this);
        vistaRegis.btnLimpiar.addActionListener(this);
        vistaRegis.passContraseña2.addActionListener(this);
        vistaRegis.passVerificarContraseña.addActionListener(this);
        vistaRegis.txtUsuario2.addActionListener(this);
    }

    private void iniciarVista() {
        try {
            vistaList.tbtusuario.setModel(ModelbdUsuarios.listar());
            vistaIngre.setTitle("Ingresar");
            vistaIngre.setSize(400, 400);
            vistaIngre.setVisible(true);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(vistaIngre, "Error al iniciar la vista: " + ex.getMessage());
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vistaIngre.btnIngresar) {
            String username = vistaIngre.txtUsuario.getText();
            String password = String.valueOf(vistaIngre.passContraseña.getPassword());

            // Realizar validación de inicio de sesión aquí
            if (ModelbdUsuarios.validarCredenciales(username, password)) {
                JOptionPane.showMessageDialog(vistaIngre, "Bienvenido: " + username);
                vistaList.setVisible(true);
                vistaIngre.setVisible(false);
            } else {
                // Credenciales inválidas, mostrar mensaje de error
                JOptionPane.showMessageDialog(vistaIngre, "Credenciales inválidas", "Error de inicio de sesión", JOptionPane.ERROR_MESSAGE);
            }
        } else if (e.getSource() == vistaIngre.btnRegistrar) {
            vistaIngre.setVisible(false);
            vistaRegis.setVisible(true);

        } else if (e.getSource() == vistaList.btnCerrar2) {
            if (JOptionPane.showConfirmDialog(vistaList, "¿Desea cerrar?", "Cerrar", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                vistaList.setVisible(false);
                vistaList.dispose();
                System.exit(0);
            }
        } else if (e.getSource() == vistaRegis.btnGuardar) {
            String password1 = vistaRegis.passContraseña2.getText();
            String password2 = vistaRegis.passVerificarContraseña.getText();
            String correo = vistaRegis.TxtCorreoElectronico.getText();
            String usuario = vistaRegis.txtUsuario2.getText();

            if (password1.equals(password2)) {
                if (correo.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "El campo de correo electrónico está vacío.", "Error", JOptionPane.ERROR_MESSAGE);
                } else if (usuario.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "El campo de nombre de usuario está vacío.", "Error", JOptionPane.ERROR_MESSAGE);
                } else if (password1.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "El campo de contraseña está vacío.", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    // Verificar si el usuario ya existe en la base de datos
                    if (ModelbdUsuarios.usuarioExiste(usuario)) {
                        JOptionPane.showMessageDialog(null, "El usuario ya existe en la base de datos.", "Error", JOptionPane.ERROR_MESSAGE);
                    } else {
                        ModelUs.setContra(password1);
                        ModelUs.setCorreo(correo);
                        ModelUs.setNombre(usuario);

                        if (this.isInsertar) {
                            try {
                                if (this.ModeldbManejador.conectar()) {
                                    ModelbdUsuarios.insertar(ModelUs);
                                    vistaList.tbtusuario.setModel(ModelbdUsuarios.listar());
                                    JOptionPane.showMessageDialog(vistaRegis, "Se agregó correctamente");
                                    limpiar();
                                    vistaRegis.setVisible(false);
                                    vistaIngre.setVisible(true);
                                    this.ModeldbManejador.desconectar();
                                } else {
                                    JOptionPane.showMessageDialog(vistaRegis, "No se pudo establecer la conexión con la base de datos.", "Error", JOptionPane.ERROR_MESSAGE);
                                }
                            } catch (Exception ex) {
                                JOptionPane.showMessageDialog(vistaRegis, "Error al agregar: " + ex.getMessage());
                                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Las contraseñas no son las mismas.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else if (e.getSource()
                == vistaList.btnCerrar2) {
            if (JOptionPane.showConfirmDialog(vistaList, "¿Desea cerrar?", "Cerrar", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                vistaList.setVisible(false);
                vistaList.dispose();
                System.exit(0);
            }
        } else if (e.getSource() == vistaRegis.btnLimpiar) {
            limpiar();
        } else if (e.getSource() == vistaRegis.btnCerrar) {
            if (JOptionPane.showConfirmDialog(vistaList, "¿Desea cerrar?", "Cerrar", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                vistaList.setVisible(false);
                vistaList.dispose();
                System.exit(0);
            }
        }

    }

    public void limpiar() {
        vistaRegis.TxtCorreoElectronico.setText("");
        vistaRegis.passContraseña2.setText("");
        vistaRegis.passVerificarContraseña.setText("");
        vistaRegis.txtUsuario2.setText("");
    }

    public static void main(String[] args) throws Exception {
        dbUsuarios db = new dbUsuarios();
        dbManejador cot = new dbManejador();
        vistaLista vista2 = new vistaLista(new JFrame(), false);
        vistaRegistro vista3 = new vistaRegistro(new JFrame(), false);
        vistaIngreso vista = new vistaIngreso(new JFrame(), true);
        Modelo.Usuarios usuarios = new Usuarios();
        Controlador con = new Controlador(vista, vista2, vista3, usuarios, cot, db);
        con.iniciarVista();
    }
}