
package Modelo;

import javax.swing.table.DefaultTableModel;

public interface dbPersistencia {
    public void insertar(Object objecto) throws Exception;
    public DefaultTableModel listar()throws Exception;
    public Object buscar(int codigo)throws Exception;
}
