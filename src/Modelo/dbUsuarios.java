/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;
import Vista.vistaRegistro;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author bmfil
 */
public class dbUsuarios extends dbManejador implements dbPersistencia {
    
    vistaRegistro vista;

public boolean usuarioExiste(String usuario) {
    boolean existe = false;

    try {
        // Realizar la conexión a la base de datos
        if (this.conectar()) {
            // Seleccionar la base de datos
            String databaseName = "sistema";
            String selectDatabaseQuery = "USE " + databaseName;
            this.sqlConsulta = conexion.prepareStatement(selectDatabaseQuery);
            this.sqlConsulta.executeUpdate();

            // Preparar la consulta SQL
            String consulta = "SELECT COUNT(*) FROM usuarios WHERE nombre = ?";
            this.sqlConsulta = conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, usuario);

            // Ejecutar la consulta
            ResultSet resultSet = this.sqlConsulta.executeQuery();

            // Obtener el resultado
            if (resultSet.next()) {
                int count = resultSet.getInt(1);
                existe = (count > 0);
            }

            // Cerrar los recursos
            resultSet.close();
            this.desconectar();
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }

    return existe;
}


    public boolean validarCredenciales(String username, String password) {
        String consulta = "SELECT COUNT(*) FROM sistema.usuarios WHERE nombre = ? AND contraseña = ?";

        try (PreparedStatement pstmt = conexion.prepareStatement(consulta)) {
            pstmt.setString(1, username);
            pstmt.setString(2, password);

            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs.next()) {
                    int count = rs.getInt(1);
                    return count > 0; // Si el recuento es mayor a 0, las credenciales son válidas
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error al validar las credenciales: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }

        return false; // Si ocurre algún error, se consideran las credenciales como inválidas
    }

    @Override
    public void insertar(Object objeto) throws Exception {
        Usuarios user = (Usuarios) objeto;

        String consulta = "INSERT INTO sistema.usuarios(nombre, correo, contraseña) VALUES (?, ?, ?)";

        if (this.conectar()) {
            try {
                PreparedStatement statement = conexion.prepareStatement(consulta);
                statement.setString(1, user.getNombre());
                statement.setString(2, user.getCorreo());
                statement.setString(3, user.getContra());

                statement.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(vista, "Surgió un error al insertar: " + e.getMessage());
            }
        } else {
            JOptionPane.showMessageDialog(vista, "No fue posible conectarse");
        }
    }

    @Override
    public DefaultTableModel listar() throws Exception {
        if (this.conectar()) {
            try {
                String sql = "select * from sistema.usuarios order by idusuarios";
                DefaultTableModel Tabla = new DefaultTableModel();
                PreparedStatement pst = conexion.prepareStatement(sql);
                ResultSet rs = pst.executeQuery();
                ResultSetMetaData Datos = rs.getMetaData();

                //Agregar Columnas
                for (int column = 1; column <= Datos.getColumnCount(); column++) {
                    Tabla.addColumn(Datos.getColumnLabel(column));
                }
                //Agregar Tablas
                while (rs.next()) {
                    Object[] row = new Object[Datos.getColumnCount()];
                    for (int column = 1; column <= Datos.getColumnCount(); column++) {
                        row[column - 1] = rs.getString(column);
                    }
                    Tabla.addRow(row);
                }
                return Tabla;
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(vista, "Surgio un error al listar" + e.getMessage());
            }
        }
        return null;
    }

    @Override
    public Object buscar(int codigo) throws Exception {
        Usuarios user = new Usuarios();
        if (this.conectar()) {
            try {
                String consultar = "Select * from sistema.usuarios where idusuarios= ?";
                this.sqlConsulta = this.conexion.prepareStatement(consultar);
                this.sqlConsulta.setInt(1, codigo);
                this.Registros = this.sqlConsulta.executeQuery();
                if (this.Registros.next()) {
                    user.setIdUsuarios(this.Registros.getInt("idusuarios"));
                    user.setNombre(this.Registros.getString("nombre"));
                    user.setCorreo(this.Registros.getString("correo"));
                    user.setContra(this.Registros.getString("contraseña"));
                }
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(vista, "Surgio un error al buscar" + e.getMessage());
            }

        }
        this.desconectar();
        return user;
    }
}
